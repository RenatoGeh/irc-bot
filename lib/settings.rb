# frozen_string_literal: true

require 'yaml'

module ObserveChat
  # Settings
  # Factory for I/O settings from configuration file.
  class Settings
    YAML_ROOT = ['observe_chat'].freeze

    SETTINGS = (YAML_ROOT + ['settings']).freeze
    IRC = (YAML_ROOT + ['irc']).freeze
    DATABASE = (YAML_ROOT + ['database']).freeze

    # Options
    # What Settings spits out.
    class Options
      def find(path)
        @cfg_file.dig(*path)
      end

      def initialize(fpath)
        @cfg_file = YAML.load_file(fpath) || {} if File.exist?(fpath)
        raise ArgumentError, "Problem parsing #{fpath}" if @cfg_file.empty?

        @set = find(SETTINGS)
        @irc = {}
        find(IRC).each { |k, v| @irc[k] = v }
        @db = find(DATABASE)
      end

      def settings(key = nil)
        key.nil? ? @set.clone : @set[key]
      end

      def register_channel(server, channel)
        c = true
        unless (s = @irc.key?(server)) && (c = @irc[server].include?(channel))
          @irc[server] = [] unless s
          @irc[server].append(channel)
        end
        [s, c]
      end

      def drop_channel(server, channel)
        if @irc.key?(server)
          i = @irc[server].index(channel)
          channels = @irc[server]
          channels.delete_at(i) unless i.nil?
          @irc.delete(server) if channels.empty?
          return !i.nil?
        end
        False
      end

      def drop_server(server)
        @irc.delete(server)
      end

      def irc(key = nil)
        key.nil? ? @irc.clone : @irc[key]
      end

      def database(key = nil)
        key.nil? ? @db.clone : @db[key]
      end
    end
    private_constant :Options

    def self.parse(path)
      Options.new(path)
    end
  end
end
