# ObserveChat

An IRC bot logger.

See the [wiki](https://gitlab.com/flusp/msfloss/msfloss/wikis/IRC-Bot-Requirements)
for more information.

## Installation

Install dependencies:

    $ bundle install

Have MongoDB running on your machine.

## Usage

To start the bot:

    $ bin/observe_chat -s --setting examples/observe_chat.yml

Will run the bot with a REST API running on the background.

    $ curl 127.0.0.1:4567/messages?nick=user-nick&channel=%23channel&from=20190601&to=20190701

To test the GET REST API.
